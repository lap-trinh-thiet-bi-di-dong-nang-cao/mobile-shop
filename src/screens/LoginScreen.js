import React, { useState } from "react";
import {
    Image,
    StatusBar,
    StyleSheet,
    Text,
    Platform,
    TouchableOpacity,
    View,
    Dimensions,
} from "react-native";

import { VisibilityOff } from "../icons/VisibilityOff";
import { Visibility } from "../icons/Visibility";
import images from "../images";
import { HInput } from "../components/HInput";
import HButton from "../components/HButton";
import { useAppContext } from "../hooks/useAppContext";

const marginTop = Platform.OS === "ios" ? 0 : StatusBar.currentHeight;
const height = Dimensions.get("window").height + marginTop;

export default function LoginScreen({ navigation }) {
    const [isChecked, setChecked] = useState(false);
    const { setConnection } = useAppContext();

    const COLOR_BORDER = "#aaa";
    const COLOR_ICON = "#aaa";
    const RADIUS = 8;

    return (
        <View style={styles.container}>
            <View style={styles.topWrapper}>
                <Text style={styles.title}>Hello Again!</Text>
                <Text style={styles.subTitle}>
                    Wellcome back you've been missed!
                </Text>
            </View>

            <View style={styles.formWrapper}>
                <HInput
                    placeholder="Enter your email"
                    borderColor={COLOR_BORDER}
                    borderWidth={1}
                    radius={RADIUS}
                    style={[styles.input, styles.emailInput]}
                />
                <HInput
                    placeholder="Password"
                    borderColor={COLOR_BORDER}
                    borderWidth={1}
                    radius={RADIUS}
                    isPassword={!isChecked}
                    endIcon={
                        isChecked ? (
                            <Visibility color={COLOR_ICON} />
                        ) : (
                            <VisibilityOff color={COLOR_ICON} />
                        )
                    }
                    handleEndIcon={() => setChecked(!isChecked)}
                    style={[styles.input, styles.passwordInput]}
                />

                <TouchableOpacity
                    style={styles.forgotPass}
                    onPress={() => navigation.navigate("ForgotPassword")}
                >
                    <Text style={styles.forgotPassLabel}>Forgot password?</Text>
                </TouchableOpacity>

                <HButton
                    text="Sign In"
                    bgColor="#FA6A69"
                    textColor="#fff"
                    fontWeight="bold"
                    radius={RADIUS}
                    style={[styles.signInBtn]}
                    onPress={() => {
                        setConnection(true);
                    }}
                />
            </View>

            <View style={styles.bottomWrapper}>
                <Text style={styles.moreOptionLabel}>- Or continue with -</Text>

                <View style={styles.moreOptions}>
                    <TouchableOpacity style={styles.option}>
                        <Image
                            style={[styles.imageLogo]}
                            source={images.google}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.option}>
                        <Image
                            style={[styles.imageLogo]}
                            source={images.apple}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.option}>
                        <Image
                            style={[styles.imageLogo]}
                            source={images.facebook}
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.signUpWrapper}>
                    <Text style={styles.signUpLabel1}>
                        Don't have an account yet?
                    </Text>
                    <TouchableOpacity
                        style={styles.signUpBtn}
                        onPress={() => navigation.navigate("SignUp")}
                    >
                        <Text style={styles.signUpLabel2}>Register now</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        height: height,
        backgroundColor: "#FCFCFE",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",

        paddingHorizontal: 32,
        paddingTop: 16,
        paddingBottom: 32,
    },
    topWrapper: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",

        marginTop: marginTop,
    },
    title: {
        fontSize: 28,
        fontWeight: "bold",
        color: "#555",
    },
    subTitle: {
        fontSize: 18,
        color: "#555",
    },

    formWrapper: {
        flex: 2,
        width: "100%",
        // paddingHorizontal: 32,
        // paddingVertical: 16,
        zIndex: 2,
    },

    input: {
        padding: 10,
        marginBottom: 10,
    },
    emailInput: {
        color: "red",
    },
    passwordInput: {},

    signInBtn: {
        marginTop: 24,
    },

    forgotPass: {
        marginLeft: 4,
    },
    forgotPassLabel: {
        color: "#22A4F1",
    },

    bottomWrapper: {
        width: "100%",

        flexDirection: "column",
        alignItems: "center",
        // backgroundColor: "green",
    },
    moreOptionLabel: {
        fontSize: 16,
        fontWeight: "bold",
    },
    moreOptions: {
        width: "80%",
        flexDirection: "row",
        justifyContent: "space-around",
        marginVertical: 16,
    },
    option: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: 70,
        paddingVertical: 8,
        borderRadius: 8,
        backgroundColor: "#eee",
    },

    imageLogo: {
        width: 32,
        height: 32,
        resizeMode: "contain",
    },

    signUpWrapper: {
        flexDirection: "row",
        marginTop: 32,
    },
    signUpLabel1: {
        color: "#555",
    },
    signUpBtn: {},
    signUpLabel2: {
        marginLeft: 8,
        color: "#22A4F1",
    },
});
