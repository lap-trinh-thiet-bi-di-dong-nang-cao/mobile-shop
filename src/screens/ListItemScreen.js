import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
} from "react-native";
import { AntDesign, Octicons } from "@expo/vector-icons";
import ItemSale from "../components/ItemSale";
import ScreenHeader from "../components/ScreenHeader";

function ListItemScreen({ navigation, route }) {
  const list = [...route.params.list];

  return (
    <View style={styles.container}>
      <ScreenHeader
        Title={() => {
          return (
            <Text style={styles.textHeader}>
              Pro<Text style={{ color: "#2C5BF6" }}>Tech</Text>
            </Text>
          );
        }}
        RightContent={() => {
          return (
            <TouchableOpacity style={styles.buttonHeader}>
              <Octicons name="bell-fill" size={24} color="black" />
            </TouchableOpacity>
          );
        }}
      />
      <View style={styles.content}>
        <Text style={styles.title}>{route.params.title}</Text>
        <SafeAreaView style={styles.listItem}>
          <FlatList
            data={list}
            renderItem={({ item, index }) => {
              return (
                <ItemSale
                  item={item}
                  key={`ListItem-${item.name}-${item.index}`}
                />
              );
            }}
          />
        </SafeAreaView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: "100%",
    height: 80,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    paddingTop: 30,
    paddingHorizontal: 20,
  },
  buttonHeader: {},
  textHeader: {
    fontSize: 18,
    fontWeight: "700",
  },
  content: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  title: {
    fontSize: 22,
    fontWeight: "700",
  },
  listPopular: {
    width: "100%",
    height: 200,
    paddingTop: 10,
  },
  listItem: {
    paddingTop: 15,
  },
});

export default ListItemScreen;
