import React, { useEffect, useRef, useState } from "react";
import { Image, ScrollView, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { ScrollToBottomButton } from "../components/ScrollToTopButton";
import Tag from "../components/Tag";
import { Filter } from "../icons/Filter";
import { SearchProductScreenHeader, Spacer } from "./SearchProductScreen";
import { ProductItem } from "../components/ProductItem";
import { data } from "../utils/utils";

const ResultProductScreen = ({ navigation }) => {
  const { bottom } = useSafeAreaInsets();
  const [scrollToBottomButtonVisible, setScrollToBottomButtonVisible] =
    useState(false);
  const scrollViewRef = useRef();

  const RightContent = () => {
    return (
      <TouchableOpacity
        style={{
          width: 50,
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "flex-end",
        }}
      >
        <Filter fill="#2C5BF6" />
        <Text style={{ fontSize: 10, color: "#2C5BF6" }}>Lọc</Text>
      </TouchableOpacity>
    );
  };

  const [selectedIndex, setSelectedIndex] = useState(0);

  const tagItem = [
    "Tất cả",
    "Điện thoại",
    "Phụ kiện",
    "Hàng cũ",
    "Máy tính bảng",
    "Đồng hồ thông minh",
    "Âm thanh",
  ];

  const [selectedIndexI, setSelectedIndexI] = useState(0);

  const tagItemI = [
    { title: "Liên quan" },
    {
      title: "Giá cao",
      Icon: () => <Image source={require("../../assets/sort_by_down.png")} />,
    },
    {
      title: "Giá thấp",
      Icon: () => <Image source={require("../../assets/sort_by_up.png")} />,
    },
  ];

  const handleScroll = (event) => {
    const offset = event.nativeEvent.contentOffset.y;

    const isScrollAtBottom = offset <= 100;
    const showScrollToBottomButton = !isScrollAtBottom;

    setScrollToBottomButtonVisible(showScrollToBottomButton);
  };

  const goToBottom = async () => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollTo({
        offset: 0,
      });
    }
    setScrollToBottomButtonVisible(false);
  };

  return (
    <View style={styles.container}>
      <SearchProductScreenHeader RightContent={RightContent} />
      <ScrollView
        style={{ flex: 1 }}
        onScroll={handleScroll}
        scrollEventThrottle={1}
        ref={scrollViewRef}
      >
        <View style={{ marginVertical: 5 }}>
          <Text style={{ fontSize: 12, color: "#666", textAlign: "center" }}>
            <Text>Tìm thấy</Text>
            <Text style={{ fontWeight: "bold" }}> 23</Text>
            <Text> sản phẩm cho từ khóa</Text>
            <Text style={{ fontWeight: "bold" }}>'Iphone'</Text>
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            paddingHorizontal: 8,
          }}
        >
          {tagItem.map((value, index) => {
            return (
              <Tag
                text={value}
                key={`${value}__${index}`}
                isSelected={selectedIndex === index}
                onPress={() => {
                  setSelectedIndex(index);
                }}
              />
            );
          })}
        </View>
        <View style={{ paddingHorizontal: 8, marginVertical: 8 }}>
          <Text style={{ fontSize: 16, fontWeight: "bold", color: "#333" }}>
            Sắp xếp theo
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            paddingHorizontal: 8,
          }}
        >
          {tagItemI.map((value, index) => {
            return (
              <Tag
                text={value.title}
                key={`${value.title}__${index}`}
                isSelected={selectedIndexI === index}
                Icon={value.Icon}
                onPress={() => {
                  setSelectedIndexI(index);
                }}
              />
            );
          })}
        </View>
        <Spacer />
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            justifyContent: "space-evenly",
            flexWrap: "wrap",
          }}
        >
          {data.map((value, index) => {
            return (
              <ProductItem
                item={value}
                key={`${value.name}__${index}`}
                onPress={() => {
                  navigation.push("Detail", {
                    data: value,
                  });
                }}
              />
            );
          })}
        </View>
        <Spacer style={{ height: bottom }} />
      </ScrollView>
      <ScrollToBottomButton
        onPress={goToBottom}
        showNotification={scrollToBottomButtonVisible}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
});

export default ResultProductScreen;
