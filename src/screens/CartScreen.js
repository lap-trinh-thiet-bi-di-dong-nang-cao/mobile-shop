import { useBottomTabBarHeight } from "@react-navigation/bottom-tabs";
import { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import CartItem from "../components/CartItem";
import ScreenHeader from "../components/ScreenHeader";

function CartScreen({ navigation }) {
  const [data, setData] = useState([...DATA]);
  const [totalPrice, setTotalPrice] = useState(0);

  const bottom = useBottomTabBarHeight();

  useEffect(() => {
    setTotalPrice(
      data.reduce(
        (total, item) => (total = total + item.product.price * item.quantity),
        0
      )
    );
  });

  const handleRemove = (index) => {
    setData(data.filter((item, i) => index != i));
  };
  const handleChangeQuantity = (index, quantity) => {
    data[index].quantity = quantity;
    setTotalPrice(
      data.reduce(
        (total, item) => (total = total + item.product.price * item.quantity),
        0
      )
    );
  };
  return (
    <View style={[styles.wrapper, { paddingBottom: bottom }]}>
      <ScreenHeader LeftContent={View} titleText="Cart" />
      <View style={styles.list}>
        <FlatList
          data={data}
          renderItem={({ item, index }) => (
            <CartItem
              onRemove={handleRemove}
              onChange={handleChangeQuantity}
              key={`${item.id}-${item.name}-${item.index}`}
              index={index}
              item={item}
            ></CartItem>
          )}
          style={{ paddingHorizontal: 10 }}
        ></FlatList>
      </View>
      <View style={styles.cast}>
        <View style={styles.fee}>
          <Text style={{ fontSize: 20 }}>Total</Text>
          <Text style={{ fontSize: 20, color: "#3399ff", fontWeight: "700" }}>
            ${totalPrice.toFixed(2)}
          </Text>
        </View>
        <TouchableOpacity style={styles.btnCast}>
          <Text style={{ fontSize: 20, color: "white", fontWeight: "700" }}>
            Buy Now
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: "#F8F8F8",
  },
  list: {
    flex: 7,
    paddingVertical: 10,
  },
  cast: {
    flex: 1,
    flexDirection: "row",
    padding: 15,
  },
  fee: {
    flex: 1,
    justifyContent: "space-evenly",
  },
  btnCast: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: "80%",
    backgroundColor: "#3399ff",
    borderRadius: 15,
  },
});
const DATA = [
  {
    quantity: 2,
    product: {
      id: 1,
      price: 299.99,
      name: "Galaxy Note 100",
      rating: 4.9,
      colors: [
        { color: "red", bonus: 0 },
        { color: "yellow", bonus: 5 },
        { color: "black", bonus: 5 },
        { color: "white", bonus: 5 },
      ],
      storages: [
        { vol: "16GB", bonus: 0 },
        { vol: "32GB", bonus: 30 },
        { vol: "64GB", bonus: 35 },
      ],
      images: [
        require("../../assets/realme_blue.jpeg"),
        require("../../assets/iphone14_xanh.jpeg"),
        require("../../assets/realme-5i.jpg"),
      ],
    },
  },
  {
    quantity: 2,
    product: {
      id: 1,
      price: 299.99,
      name: "Galaxy Note 100",
      rating: 4.9,
      colors: [
        { color: "red", bonus: 0 },
        { color: "yellow", bonus: 5 },
        { color: "black", bonus: 5 },
        { color: "white", bonus: 5 },
      ],
      storages: [
        { vol: "16GB", bonus: 0 },
        { vol: "32GB", bonus: 30 },
        { vol: "64GB", bonus: 35 },
      ],
      images: [
        require("../../assets/realme_blue.jpeg"),
        require("../../assets/iphone14_xanh.jpeg"),
        require("../../assets/realme-5i.jpg"),
      ],
    },
  },
  {
    quantity: 2,
    product: {
      id: 1,
      price: 299.99,
      name: "Galaxy Note 100",
      rating: 4.9,
      colors: [
        { color: "red", bonus: 0 },
        { color: "yellow", bonus: 5 },
        { color: "black", bonus: 5 },
        { color: "white", bonus: 5 },
      ],
      storages: [
        { vol: "16GB", bonus: 0 },
        { vol: "32GB", bonus: 30 },
        { vol: "64GB", bonus: 35 },
      ],
      images: [
        require("../../assets/realme_blue.jpeg"),
        require("../../assets/iphone14_xanh.jpeg"),
        require("../../assets/realme-5i.jpg"),
      ],
    },
  },
];
export default CartScreen;
