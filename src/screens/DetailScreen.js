import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { AntDesign, Entypo, EvilIcons } from "@expo/vector-icons";
import ScreenHeader from "../components/ScreenHeader";
import { useState } from "react";
import PickerButton from "../components/PickerButton";
import PickerButtonSquad from "../components/PickerButtonSquad";
import { useOverlayContext } from "../contexts/OverlayContext";
import { useImageGalleryContext } from "../contexts/ImageGalleryContext";
let PRODUCT = {
  id: 1,
  price: 299.99,
  name: "Galaxy Note 100",
  rating: 4.9,
  colors: [
    { color: "red", bonus: 0 },
    { color: "yellow", bonus: 5 },
    { color: "black", bonus: 5 },
    { color: "white", bonus: 5 },
  ],
  storages: [
    { vol: "16GB", bonus: 0 },
    { vol: "32GB", bonus: 30 },
    { vol: "64GB", bonus: 35 },
  ],
  images: [
    require("../../assets/realme_blue.jpeg"),
    require("../../assets/iphone14_xanh.jpeg"),
    require("../../assets/realme-5i.jpg"),
  ],
};

function DetailScreen({
  navagation,
  route: {
    params: { data },
  },
}) {
  PRODUCT = data || PRODUCT;
  const [currentImage, setCurrentImage] = useState(0);
  const { setOverlay } = useOverlayContext();
  const { setImages, setName } = useImageGalleryContext();
  const [colorActive, setColorActive] = useState(0);
  const [storageActive, setStorageActive] = useState(0);

  const [price, setPrice] = useState(
    PRODUCT.price +
    PRODUCT.colors[colorActive].bonus +
    PRODUCT.storages[storageActive].bonus
  );

  const handleChangePrice = () => {
    setPrice(
      PRODUCT.price +
      PRODUCT.colors[colorActive].bonus +
      PRODUCT.storages[storageActive].bonus
    );
  };

  return (
    <View style={styles.wrapper}>
      <ScreenHeader
        onBack={() => console.log("hello")}
        titleText="Detail"
        RightContent={() => (
          <TouchableOpacity>
            <AntDesign name="sharealt" size={20} color="black" />
          </TouchableOpacity>
        )}
      ></ScreenHeader>
      <View style={styles.images}>
        <TouchableOpacity
          onPress={() => {
            setName(data.name);
            setImages(data.images);
            setOverlay("gallery");
          }}
        >
          <Image
            style={styles.image}
            resizeMode="contain"
            source={PRODUCT.images[currentImage]}
          // source={{
          //   uri: images[currentImage],
          // }}
          />
        </TouchableOpacity>
        <View style={styles.dot}>
          {PRODUCT.images.map((img, index) => (
            <TouchableOpacity
              style={styles.btnChangeImage}
              key={index}
              onPress={() => setCurrentImage(index)}
            >
              <Entypo
                name="dot-single"
                size={31}
                color={currentImage == index ? "#2C5BF6" : "gray"}
              />
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <View style={styles.body}>
        <View style={styles.line}></View>
        <View style={styles.mobileGroup}>
          <Text style={styles.mobileName}>{PRODUCT.name}</Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Entypo name="star" size={24} color="#ffcc00" />
            <Text style={styles.rateText}>{PRODUCT.rating}</Text>
          </View>
        </View>
        <Text style={styles.textPrice}>${price}</Text>
        <View style={styles.colorGroup}>
          <Text style={styles.textColor}>Color</Text>
          <View style={styles.colorPickerGroup}>
            {PRODUCT.colors.map((color, index) => (
              <PickerButton
                key={index}
                color={color.color}
                onPressIn={() => setColorActive(index)}
                onPressOut={() => handleChangePrice()}
                active={colorActive == index}
              ></PickerButton>
            ))}
          </View>
        </View>
        <View style={styles.storageGroup}>
          <Text style={styles.textStorage}>Storage Space</Text>
          <View style={styles.storagePicker}>
            {PRODUCT.storages.map((storage, index) => (
              <PickerButtonSquad
                text={storage.vol}
                onPressIn={() => setStorageActive(index)}
                onPressOut={() => handleChangePrice()}
                active={storageActive == index}
                key={`${storage.bonus}-${index}`}
              ></PickerButtonSquad>
            ))}
          </View>
        </View>

        <View style={styles.footer}>
          <TouchableOpacity style={styles.btnTym}>
            <EvilIcons name="heart" size={40} color="#3399ff" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnAddToCart}>
            <Text style={styles.btnTextAddCart}>Add To Cart</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: "#F8F8F8",
  },
  images: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    height: "85%",
  },

  dot: {
    flexDirection: "row",
  },
  btnChangeImage: {
    padding: 2,
  },
  body: {
    flex: 3,
    padding: 20,
    paddingTop: 40,
    backgroundColor: "white",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,

    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.5,
    shadowRadius: 16.0,

    elevation: 24,
  },
  line: {
    width: "20%",
    height: 5,
    backgroundColor: "gray",
    alignSelf: "center",
    position: "relative",
    borderRadius: 5,
    opacity: 0.5,
    top: -28,
  },
  mobileName: {
    marginBottom: 10,
    fontSize: 24,
    fontWeight: "bold",
  },
  mobileGroup: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  rateText: {
    color: "gray",
    fontSize: 20,
  },
  textPrice: {
    fontSize: 24,
    color: "#3399ff",
    fontWeight: "700",
  },
  colorGroup: {
    marginTop: 20,
    height: "25%",
  },
  textColor: {
    fontSize: 24,
    marginBottom: 15,
  },
  colorPickerGroup: {
    flexDirection: "row",
    alignItems: "center",
    height: "20%",
  },
  storageGroup: {
    height: "25%",
  },
  textStorage: {
    fontSize: 24,
    marginBottom: 15,
  },
  storagePicker: {
    flexDirection: "row",
  },
  footer: {
    position: "relative",
    top: 10,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "black",
  },
  btnTym: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    height: "70%",
    borderRadius: 15,
    borderWidth: 1,
    marginRight: 10,
    borderColor: "#3399ff",
  },
  btnAddToCart: {
    height: "70%",
    flex: 7,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#3399ff",
    borderRadius: 15,
  },
  btnTextAddCart: {
    color: "white",
    fontWeight: "500",
    fontSize: 24,
  },
});
export default DetailScreen;
