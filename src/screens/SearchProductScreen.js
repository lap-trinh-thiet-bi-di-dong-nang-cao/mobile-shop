import { useNavigation } from "@react-navigation/native";
import React from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import Button from "../components/Button";
import ScreenHeader from "../components/ScreenHeader";
import Tag from "../components/Tag";
import { Search } from "../icons/Search";

export const SearchProductScreenHeader = ({ RightContent }) => {
  const navigation = useNavigation();
  const CenterContent = () => {
    return (
      <View style={styles.searchInputContainer}>
        <TextInput
          style={styles.searchInput}
          placeholder="Search"
          clearButtonMode="while-editing"
        />
      </View>
    );
  };

  const SearchButton = () => {
    return (
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => {
          navigation.push("ResultProduct");
        }}
      >
        <Search fill="#fff" width={20} />
      </TouchableOpacity>
    );
  };

  return (
    <ScreenHeader
      CenterContent={CenterContent}
      RightContent={RightContent ? RightContent : SearchButton}
      rightWrapContainer={{ width: null, height: null }}
    />
  );
};

export const Spacer = ({ style }) => {
  return (
    <View
      style={{
        height: 10,
        ...style,
      }}
    />
  );
};

const SearchProductScreen = ({ navigation }) => {
  const recomendation = [
    "iPhone 14 Pro",
    "iPhone 14 Pro max",
    "iPhone 14",
    "Samsung Galaxy S3",
    "Samsung Fold 7",
  ];

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={styles.rowRsContainer}
        onPress={() => {
          navigation.push("ResultProduct");
        }}
      >
        <Text style={{ fontSize: 12 }}>{item}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <SearchProductScreenHeader />
      <Spacer />
      <View style={styles.recommendContainer}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "#eee",
            paddingVertical: 8,
            paddingHorizontal: 5,
          }}
        >
          <Text style={{ fontSize: 12, fontWeight: "500" }}>
            Có Thể Bạn Muốn Tìm
          </Text>
        </View>
        <View style={{ flexDirection: "row", padding: 8, flexWrap: "wrap" }}>
          {recomendation.map((value, index) => {
            return <Tag key={`${value}__${index}`} text={value} />;
          })}
        </View>
      </View>
      <Spacer />
      <View style={styles.rsContainer}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "#eee",
            paddingVertical: 8,
            paddingHorizontal: 5,
          }}
        >
          <Text style={{ fontSize: 12, fontWeight: "500" }}>iPhone 14</Text>
        </View>
        <FlatList data={recomendation} renderItem={renderItem} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchInputContainer: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#2C5BF6",
    height: 30,
    alignItems: "center",
    paddingLeft: 8,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
  },
  searchInput: {
    fontSize: 13,
    flex: 1,
  },
  buttonContainer: {
    backgroundColor: "#2C5BF6",
    height: 30,
    width: 40,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
  },
  recommendContainer: {
    backgroundColor: "white",
  },
  rowRsContainer: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#eee",
  },
  rsContainer: {
    backgroundColor: "white",
  },
});

export default SearchProductScreen;
