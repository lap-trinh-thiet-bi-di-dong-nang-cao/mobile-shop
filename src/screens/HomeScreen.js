import { StyleSheet, View, Text, FlatList, SafeAreaView } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import {
  AntDesign,
  Octicons,
  SimpleLineIcons,
  Entypo,
  MaterialCommunityIcons,
  Ionicons,
} from "@expo/vector-icons";
import ItemPopular from "../components/ItemPopular";
import ItemSale from "../components/ItemSale";
import { useBottomTabBarHeight } from "@react-navigation/bottom-tabs";
import { data } from "../utils/utils";
import ScreenHeader from "../components/ScreenHeader";
import { useNavigation } from "@react-navigation/native";

function HomeScreen() {
  const bottom = useBottomTabBarHeight();

  const navigation = useNavigation();

  return (
    <View style={[styles.container, { paddingBottom: bottom }]}>
      <ScreenHeader
        LeftContent={() => {
          return (
            <TouchableOpacity style={styles.buttonHeader}>
              <AntDesign name="appstore1" size={24} color="black" />
            </TouchableOpacity>
          );
        }}
        Title={() => {
          return (
            <Text style={styles.textHeader}>
              Pro<Text style={{ color: "#2C5BF6" }}>Tech</Text>
            </Text>
          );
        }}
        RightContent={() => {
          return (
            <TouchableOpacity style={styles.buttonHeader}>
              <Octicons name="bell-fill" size={24} color="black" />
            </TouchableOpacity>
          );
        }}
      />

      <View style={styles.body}>
        <View style={styles.category}>
          <Text
            style={{ fontSize: 18, fontWeight: "700", paddingHorizontal: 10 }}
          >
            Category
          </Text>
          <View style={styles.listCategory}>
            <TouchableOpacity style={styles.buttonCategory}>
              <View
                style={[
                  styles.itemCategory,
                  {
                    ...Platform.select({
                      android: {
                        elevation: 4,
                      },
                      default: {
                        shadowOffset: {
                          height: 3,
                          width: 0,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 4,
                      },
                    }),
                  },
                ]}
              >
                <SimpleLineIcons
                  style={styles.icon}
                  name="screen-smartphone"
                  size={24}
                  color="black"
                />
              </View>
              <Text style={styles.textCategory}>Phone</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonCategory}>
              <View
                style={[
                  styles.itemCategory,
                  {
                    ...Platform.select({
                      android: {
                        elevation: 4,
                      },
                      default: {
                        shadowOffset: {
                          height: 3,
                          width: 0,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 4,
                      },
                    }),
                  },
                ]}
              >
                <Entypo
                  style={styles.icon}
                  name="laptop"
                  size={24}
                  color="black"
                />
              </View>
              <Text style={styles.textCategory}>Laptop</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonCategory}>
              <View
                style={[
                  styles.itemCategory,
                  {
                    ...Platform.select({
                      android: {
                        elevation: 4,
                      },
                      default: {
                        shadowOffset: {
                          height: 3,
                          width: 0,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 4,
                      },
                    }),
                  },
                ]}
              >
                <MaterialCommunityIcons
                  style={styles.icon}
                  name="monitor"
                  size={24}
                  color="black"
                />
              </View>
              <Text style={styles.textCategory}>Monitor</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonCategory}>
              <View
                style={[
                  styles.itemCategory,
                  {
                    ...Platform.select({
                      android: {
                        elevation: 4,
                      },
                      default: {
                        shadowOffset: {
                          height: 3,
                          width: 0,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 4,
                      },
                    }),
                  },
                ]}
              >
                <Ionicons
                  style={styles.icon}
                  name="print-outline"
                  size={24}
                  color="black"
                />
              </View>
              <Text style={styles.textCategory}>Printers</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonCategory}>
              <View
                style={[
                  styles.itemCategory,
                  {
                    ...Platform.select({
                      android: {
                        elevation: 4,
                      },
                      default: {
                        shadowOffset: {
                          height: 3,
                          width: 0,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 4,
                      },
                    }),
                  },
                ]}
              >
                <Entypo
                  style={styles.icon}
                  name="dots-three-horizontal"
                  size={24}
                  color="black"
                />
              </View>
              <Text style={styles.textCategory}>All</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.popular}>
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: 15,
              paddingHorizontal: 10,
            }}
          >
            <Text style={{ fontSize: 18, fontWeight: "700" }}>Popular</Text>
            <Text
              style={{
                fontSize: 16,
                color: "#2C5BF6",
              }}
              onPress={() => {
                navigation.navigate("ListItem", {
                  list: data,
                  title: "Popular",
                });
              }}
            >
              See All
            </Text>
          </View>

          <View style={styles.listPopular}>
            <FlatList
              horizontal={true}
              data={data}
              style={{ padding: 10 }}
              renderItem={({ item, index }) => {
                return (
                  <ItemPopular
                    item={item}
                    key={`${item.name}-${index}`}
                    onPress={() => {
                      navigation.navigate("Detail", {
                        data: item,
                      });
                    }}
                  />
                );
              }}
            />
          </View>
        </View>
        <View style={styles.sale}>
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: 15,
              paddingHorizontal: 10,
            }}
          >
            <Text style={{ fontSize: 18, fontWeight: "700" }}>On Sale</Text>
            <Text
              style={{
                fontSize: 16,
                color: "#2C5BF6",
              }}
              onPress={() => {
                navigation.navigate("ListItem", {
                  list: data,
                  title: "On Sale",
                });
              }}
            >
              See All
            </Text>
          </View>

          <View style={[styles.listSale]}>
            <FlatList
              data={data}
              renderItem={({ item, index }) => {
                return (
                  <ItemSale
                    item={item}
                    key={`Sale-${item.name}-${item.index}`}
                    onPress={() => {
                      navigation.navigate("Detail", {
                        data: item,
                      });
                    }}
                  />
                );
              }}
              style={{ padding: 10 }}
            />
          </View>
        </View>
      </View>
      <View>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: "100%",
    height: "13%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    paddingTop: "13%",
    paddingHorizontal: 20,
  },
  buttonHeader: {},
  textHeader: {
    fontSize: 18,
    fontWeight: "700",
  },
  body: {
    width: "100%",
    flex: 1,
  },
  category: {
    width: "100%",
    marginTop: 5,
  },
  listCategory: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 10,
    paddingHorizontal: 16,
  },
  buttonCategory: {
    width: 65,
    height: 65,
    justifyContent: "center",
    alignItems: "center",
  },
  itemCategory: {
    width: 60,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "white",
    borderRadius: 10,
  },
  textCategory: {
    color: "#666666",
    fontSize: 13,
  },
  popular: {
    width: "100%",
    height: 230,
  },
  listPopular: {
    width: "100%",
    paddingTop: 10,
  },
  sale: {
    flex: 1,
  },
  listSale: {
    width: "100%",
    flex: 1,
    paddingTop: 10,
  },
  vFooter: {
    width: "100%",
    position: "absolute",
    bottom: 0,
  },
});

export default HomeScreen;
