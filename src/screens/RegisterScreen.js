import React, { useState } from "react";
import {
  StatusBar,
  StyleSheet,
  Text,
  Platform,
  TouchableOpacity,
  View,
  ScrollView,
} from "react-native";

import HButton from "../components/HButton";
import { HInput } from "../components/HInput";
import { VisibilityOff } from "../icons/VisibilityOff";
import { Visibility } from "../icons/Visibility";

const marginTop = Platform.OS === "ios" ? 0 : StatusBar.currentHeight;
// const height = Dimensions.get("window").height + marginTop;

export default function RegisterScreen({ navigation }) {
  const [isChecked, setChecked] = useState(false);

  const COLOR_BORDER = "#aaa";
  const COLOR_ICON = "#aaa";
  const RADIUS = 8;

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.topWrapper}>
          <Text style={styles.title}>Sign Up</Text>
          <Text style={styles.subTitle}>Create an account. It's free.</Text>
        </View>

        <View style={styles.formWrapper}>
          <HInput
            placeholder="First name"
            borderColor={COLOR_BORDER}
            borderWidth={1}
            radius={RADIUS}
            style={[styles.input, styles.firstName]}
          />
          <HInput
            placeholder="Last name"
            borderColor={COLOR_BORDER}
            borderWidth={1}
            radius={RADIUS}
            style={[styles.input, styles.lastName]}
          />
          <HInput
            placeholder="Your email"
            borderColor={COLOR_BORDER}
            borderWidth={1}
            radius={RADIUS}
            style={[styles.input, styles.email]}
          />

          <HInput
            placeholder="Your phone"
            borderColor={COLOR_BORDER}
            borderWidth={1}
            radius={RADIUS}
            style={[styles.input, styles.phone]}
          />
          <HInput
            placeholder="Enter  password"
            borderColor={COLOR_BORDER}
            borderWidth={1}
            radius={RADIUS}
            isPassword={!isChecked}
            endIcon={
              isChecked ? (
                <Visibility color={COLOR_ICON} />
              ) : (
                <VisibilityOff color={COLOR_ICON} />
              )
            }
            handleEndIcon={() => setChecked(!isChecked)}
            style={[styles.input, styles.passwordInput]}
          />

          <HButton
            text="Sign Up"
            bgColor="#FA6A69"
            textColor="#fff"
            fontWeight="bold"
            radius={RADIUS}
            style={[styles.signUpBtn]}
            onPress={() => navigation.navigate("Login")}
          />
        </View>

        <View style={styles.bottomWrapper}>
          <View style={styles.signInWrapper}>
            <Text style={styles.signInLabel1}>Already have an account?</Text>
            <TouchableOpacity
              style={styles.signInBtn}
              onPress={() => navigation.navigate("Login")}
            >
              <Text style={styles.signInLabel2}>Sign in now</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: height,
    backgroundColor: "#FCFCFE",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",

    paddingHorizontal: 32,
    paddingTop: 4,
  },
  scrollView: {
    width: "100%",
    height: "100%",
    marginTop: marginTop,
    // backgroundColor: "red",
  },
  topWrapper: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",

    height: 150,
    marginBottom: 32,
    // backgroundColor: "red",
  },
  title: {
    fontSize: 28,
    fontWeight: "bold",
    color: "#555",
  },
  subTitle: {
    fontSize: 18,
    color: "#555",
  },

  formWrapper: {
    flex: 2,
    width: "100%",
    // paddingHorizontal: 32,
    // paddingVertical: 16,
    zIndex: 2,
  },

  input: {
    padding: 10,
    marginBottom: 10,
  },
  firtName: {},
  lastName: {},
  email: {},
  phone: {},
  passwordInput: {},

  signUpBtn: {
    marginTop: 24,
  },

  bottomWrapper: {
    flex: 1,
    width: "100%",

    flexDirection: "column",
    alignItems: "center",
    marginTop: 32,
    // backgroundColor: "green",
  },

  signInWrapper: {
    flexDirection: "row",
    marginTop: 32,
  },
  signInLabel1: {
    color: "#555",
  },
  signInBtn: {},
  signInLabel2: {
    marginLeft: 8,
    color: "#22A4F1",
  },
});
