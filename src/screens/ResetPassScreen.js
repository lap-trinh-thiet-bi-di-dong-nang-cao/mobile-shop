import React, { useState } from "react";
import {
    StatusBar,
    StyleSheet,
    Text,
    Platform,
    TouchableOpacity,
    View,
    ScrollView,
    Image,
} from "react-native";

import HButton from "../components/HButton";
import { HInput } from "../components/HInput";
import { VisibilityOff } from "../icons/VisibilityOff";
import { Visibility } from "../icons/Visibility";
import images from "../images";

const marginTop = Platform.OS === "ios" ? 0 : StatusBar.currentHeight;
// const height = Dimensions.get("window").height + marginTop;

export default function ResetPassScreen({ navigation }) {
    const [isChecked, setChecked] = useState(false);

    const COLOR_BORDER = "#aaa";
    const COLOR_ICON = "#aaa";
    const RADIUS = 8;

    return (
        <View style={styles.container}>
            {/* <ScrollView
                style={styles.scrollView}
                showsVerticalScrollIndicator={false}
            > */}
            <View style={styles.imageWrapper}>
                <Image source={images.resetPass} style={styles.image} />
            </View>
            <View style={styles.topWrapper}>
                <Text style={styles.title}>Reset password</Text>
                <Text style={styles.subTitle}>
                    Don't worry! Please enter the address associated with your
                    account.
                </Text>
            </View>

            <View style={styles.formWrapper}>
                <HInput
                    placeholder="New Password"
                    borderColor={COLOR_BORDER}
                    borderWidth={1}
                    radius={RADIUS}
                    isPassword={!isChecked}
                    endIcon={
                        isChecked ? (
                            <Visibility color={COLOR_ICON} />
                        ) : (
                            <VisibilityOff color={COLOR_ICON} />
                        )
                    }
                    handleEndIcon={() => setChecked(!isChecked)}
                    style={[styles.input, styles.passwordInput]}
                />

                <HButton
                    text="Submit"
                    bgColor="#FA6A69"
                    textColor="#fff"
                    fontWeight="bold"
                    radius={RADIUS}
                    style={[styles.submitBtn]}
                    onPress={() => navigation.navigate("Login")}
                />
            </View>
            {/* </ScrollView> */}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // height: height,
        backgroundColor: "#FCFCFE",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",

        paddingHorizontal: 32,
        paddingTop: 4,
    },
    scrollView: {
        width: "100%",
        height: "100%",
        marginTop: marginTop,
        // backgroundColor: "red",
    },
    imageWrapper: {
        flexDirection: "row",
        justifyContent: "center",
    },
    image: {
        width: 240,
        height: 240,
        resizeMode: "contain",
    },
    topWrapper: {
        // flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",

        marginBottom: 16,
        // backgroundColor: "red",
    },
    title: {
        fontSize: 28,
        fontWeight: "bold",
        color: "#555",
    },
    subTitle: {
        fontSize: 18,
        color: "#555",
    },

    formWrapper: {
        // flex: 2,
        width: "100%",
        zIndex: 2,
    },

    input: {
        padding: 10,
    },
    email: {},

    submitBtn: {
        marginTop: 16,
    },
});
