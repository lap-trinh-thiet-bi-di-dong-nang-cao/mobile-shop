import React, { useEffect, useRef, useState } from "react";
import {
    StatusBar,
    StyleSheet,
    Text,
    Platform,
    TouchableOpacity,
    View,
    ScrollView,
    Image,
} from "react-native";

import HButton from "../components/HButton";
import { HInput } from "../components/HInput";
import { VisibilityOff } from "../icons/VisibilityOff";
import { Visibility } from "../icons/Visibility";
import images from "../images";

const marginTop = Platform.OS === "ios" ? 0 : StatusBar.currentHeight;
// const height = Dimensions.get("window").height + marginTop;

export default function OTPScreen({ navigation }) {
    const [OTPvalue, setOTPvalue] = useState("");

    const COLOR_BORDER = "#aaa";
    const COLOR_ICON = "#aaa";
    const RADIUS = 8;

    return (
        <View style={styles.container}>
            {/* <ScrollView
                style={styles.scrollView}
                showsVerticalScrollIndicator={false}
            > */}
            <View style={styles.imageWrapper}>
                <Image source={images.OPT} style={styles.image} />
            </View>
            <View style={styles.topWrapper}>
                <Text style={styles.title}>OTP Verification</Text>
                <Text style={styles.subTitle}>
                    Enter OTP code sent to +84987654321
                </Text>
            </View>

            <View style={styles.formWrapper}>
                <View style={styles.inputContainer}>
                    <HInput
                        maxLength={1}
                        textAlign="center"
                        borderColor={COLOR_BORDER}
                        borderWidth={1}
                        radius={RADIUS}
                        style={[styles.input, styles.input1]}
                    />
                    <HInput
                        maxLength={1}
                        textAlign="center"
                        borderColor={COLOR_BORDER}
                        borderWidth={1}
                        radius={RADIUS}
                        style={[styles.input, styles.input1]}
                    />
                    <HInput
                        maxLength={1}
                        textAlign="center"
                        borderColor={COLOR_BORDER}
                        borderWidth={1}
                        radius={RADIUS}
                        style={[styles.input, styles.input1]}
                    />
                    <HInput
                        maxLength={1}
                        textAlign="center"
                        borderColor={COLOR_BORDER}
                        borderWidth={1}
                        radius={RADIUS}
                        style={[styles.input, styles.input1]}
                    />
                    <HInput
                        maxLength={1}
                        textAlign="center"
                        borderColor={COLOR_BORDER}
                        borderWidth={1}
                        radius={RADIUS}
                        style={[styles.input, styles.input1]}
                    />
                    <HInput
                        maxLength={1}
                        textAlign="center"
                        borderColor={COLOR_BORDER}
                        borderWidth={1}
                        radius={RADIUS}
                        style={[styles.input, styles.input1]}
                    />
                </View>

                <HButton
                    text="Verify"
                    bgColor="#FA6A69"
                    textColor="#fff"
                    fontWeight="bold"
                    radius={RADIUS}
                    style={[styles.submitBtn]}
                    onPress={() => navigation.navigate("ResetPassword")}
                />
            </View>
            <View style={styles.bottomWrapper}>
                <View style={styles.resendOTPWrapper}>
                    <Text style={styles.label1}>Don't receive OPT code?</Text>
                    <TouchableOpacity style={styles.resendBtn}>
                        <Text style={styles.label2}>Resend OTP</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {/* </ScrollView> */}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // height: height,

        backgroundColor: "#FCFCFE",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",

        paddingHorizontal: 32,
        paddingTop: 4,
    },
    scrollView: {
        width: "100%",
        height: "70%",
        marginTop: marginTop,
        // backgroundColor: "red",
    },
    imageWrapper: {
        flexDirection: "row",
        justifyContent: "center",
    },
    image: {
        width: 240,
        height: 240,
        resizeMode: "contain",
    },
    topWrapper: {
        // flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",

        // height: 150,
        marginTop: 16,
        marginBottom: 24,
        // backgroundColor: "red",
    },
    title: {
        fontSize: 28,
        fontWeight: "bold",
        color: "#555",
    },
    subTitle: {
        fontSize: 18,
        color: "#555",
    },

    formWrapper: {
        // flex: 2,
        width: "100%",
        zIndex: 2,
    },
    inputContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
    },

    input: {
        padding: 6,
        // paddingHorizontal: 12,
        width: 42,
    },
    input1: {},
    input2: {},
    input3: {},
    input4: {},
    input5: {},
    input6: {},
    email: {},

    submitBtn: {
        marginTop: 16,
    },

    bottomWrapper: {
        width: "100%",

        flexDirection: "column",
        alignItems: "center",
        // backgroundColor: "green",
    },

    resendOTPWrapper: {
        flexDirection: "row",
        marginTop: 32,
    },
    label1: {
        color: "#555",
    },
    resendBtn: {},
    label2: {
        marginLeft: 8,
        color: "#22A4F1",
    },
});
