import {
  BottomTabBar,
  createBottomTabNavigator,
} from "@react-navigation/bottom-tabs";
import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import SearchProductScreen from "./SearchProductScreen";
import { FontAwesome5 } from "@expo/vector-icons";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { TabBarAdvancedButton } from "../components/TabBarAvancedButton";
import HomeScreen from "./HomeScreen";
import CartScreen from "./CartScreen";

const Tab = createBottomTabNavigator();

const MainScreen = ({ navigation }) => {
  const inset = useSafeAreaInsets();

  return (
    <View style={styles.container}>
      <Tab.Navigator
        screenOptions={{ headerShown: false }}
        tabBar={(props) => (
          <View style={styles.navigatorContainer}>
            <BottomTabBar {...props} />
          </View>
        )}
        defaultScreenOptions={{
          showIcon: true,
          style: styles.navigator,
          tabStyle: {
            backgroundColor: "#fff",
          },

          tabBarInactiveTintColor: "#2C5BF6",
        }}
      >
        <Tab.Screen
          name={"Home"}
          component={HomeScreen}
          options={{
            tabBarIcon: ({ color }) => (
              <FontAwesome5 color={color} name={"home"} size={20} />
            ),
            tabBarShowLabel: false,
          }}
        />
        <Tab.Screen
          name={"Search"}
          component={SearchProductScreen}
          options={{
            tabBarIcon: ({ color }) => (
              <FontAwesome5 color={color} name={"search"} size={20} />
            ),
            tabBarShowLabel: false,
          }}
        />
        <Tab.Screen
          name={"Cart"}
          component={CartScreen}
          options={{
            tabBarButton: (props) => (
              <TabBarAdvancedButton bgColor={"#fff"} {...props} />
            ),
            tabBarIcon: ({ color }) => (
              <FontAwesome5 color={color} name={"shopping-bag"} size={20} />
            ),
            tabBarShowLabel: false,
          }}
        />
        <Tab.Screen
          name={"Favorite"}
          component={View}
          options={{
            tabBarIcon: ({ color }) => (
              <FontAwesome5 color={color} name={"user"} size={20} />
            ),
            tabBarShowLabel: false,
          }}
        />
        <Tab.Screen
          name={"User"}
          component={View}
          options={{
            tabBarIcon: ({ color }) => (
              <FontAwesome5 color={color} name={"home"} size={20} />
            ),
            tabBarShowLabel: false,
          }}
        />
      </Tab.Navigator>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navigatorContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  navigator: {
    borderTopWidth: 0,
    backgroundColor: "transparent",
    elevation: 30,
  },
  xFillLine: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: 34,
  },
});

export default MainScreen;
