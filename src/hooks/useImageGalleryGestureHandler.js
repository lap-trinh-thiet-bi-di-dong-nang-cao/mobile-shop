import {
  cancelAnimation,
  Easing,
  runOnJS,
  useAnimatedGestureHandler,
  useSharedValue,
  withDecay,
  withTiming,
} from "react-native-reanimated";
import { useOverlayContext } from "../contexts/OverlayContext";

export const HasPinched = {
  FALSE: 0,
  TRUE: 1,
};

export const IsSwiping = {
  UNDETERMINED: 0,
  TRUE: 1,
  FALSE: 2,
};

const MARGIN = 32;

export const useImageGalleryGestureHandler = ({
  currentImageHeight,
  halfScreenHeight,
  halfScreenWidth,
  offsetScale,
  scale,
  screenHeight,
  selectedIndex,
  translateX,
  translateY,
  translationX,
  screenWidth,
  headerFooterVisible,
  overlayOpacity,
  imageLength,
  setSelectedIndex,
}) => {
  const { setOverlay } = useOverlayContext();
  const isAndroid = Platform.OS === "android";

  const hasHitBottomScale = useSharedValue(1);
  const hasHitTopScale = useSharedValue(0);

  const originX = useSharedValue(0);
  const originY = useSharedValue(0);
  const oldFocalX = useSharedValue(0);
  const oldFocalY = useSharedValue(0);
  const focalX = useSharedValue(0);
  const focalY = useSharedValue(0);
  const index = useSharedValue(0);

  if (index.value !== selectedIndex) {
    index.value = selectedIndex;
  }

  const offsetX = useSharedValue(0);
  const offsetY = useSharedValue(0);

  const focalOffsetX = useSharedValue(0);
  const focalOffsetY = useSharedValue(0);
  const adjustedFocalX = useSharedValue(0);
  const adjustedFocalY = useSharedValue(0);
  //   const tapX = useSharedValue(0);
  //   const tapY = useSharedValue(0);

  const numberOfPinchFingers = useSharedValue(0);
  const isSwiping = useSharedValue(0);
  const isPinch = useSharedValue(false);
  const hasPinched = useSharedValue(0);

  const resetTouchValues = () => {
    "worklet";
    focalX.value = 0;
    focalY.value = 0;
    oldFocalX.value = 0;
    oldFocalY.value = 0;
    originX.value = 0;
    originY.value = 0;
    focalOffsetX.value = 0;
    focalOffsetY.value = 0;
    numberOfPinchFingers.value = 0;
    isPinch.value = false;
    isSwiping.value = IsSwiping.UNDETERMINED;
  };

  const resetMovementValues = () => {
    "worklet";
    translateX.value = 0;
    translateY.value = 0;
    scale.value = 1;
    offsetScale.value = 1;
  };

  const onPan = useAnimatedGestureHandler(
    {
      onActive: (evt) => {
        if (evt.numberOfPointers === 1 && !isPinch.value) {
          if (isAndroid && hasPinched.value === HasPinched.TRUE) {
            hasPinched.value = HasPinched.FALSE;
            isSwiping.value = IsSwiping.FALSE;
            offsetX.value = translateX.value + evt.translationX;
            offsetY.value = translateY.value - evt.translationY;
          }

          if (isSwiping.value === IsSwiping.UNDETERMINED) {
            const maxXYRatio = isAndroid ? 1 : 0.25;
            if (
              Math.abs(evt.translationX / evt.translationY) > maxXYRatio &&
              (Math.abs(-halfScreenWidth * (scale.value - 1) - offsetX.value) <
                3 ||
                Math.abs(halfScreenWidth * (scale.value - 1) - offsetX.value) <
                  3)
            ) {
              isSwiping.value = IsSwiping.TRUE;
            }
            if (Math.abs(evt.translationY) > 25) {
              isSwiping.value = IsSwiping.FALSE;
            }
          }

          const localEvtScale = scale.value / offsetScale.value;

          translateX.value =
            scale.value !== offsetScale.value
              ? offsetX.value * localEvtScale - evt.translationX
              : offsetX.value - evt.translationX;
          translateY.value =
            isSwiping.value !== IsSwiping.TRUE
              ? scale.value !== offsetScale.value
                ? offsetY.value * localEvtScale + evt.translationY
                : offsetY.value + evt.translationY
              : translateY.value;

          scale.value =
            currentImageHeight * offsetScale.value < screenHeight &&
            translateY.value > 0
              ? offsetScale.value *
                (1 - (1 / 3) * (translateY.value / screenHeight))
              : currentImageHeight * offsetScale.value > screenHeight &&
                translateY.value >
                  (currentImageHeight / 2) * offsetScale.value -
                    halfScreenHeight
              ? offsetScale.value *
                (1 -
                  (1 / 3) *
                    ((translateY.value -
                      ((currentImageHeight / 2) * offsetScale.value -
                        halfScreenHeight)) /
                      screenHeight))
              : scale.value;

          overlayOpacity.value = localEvtScale;
        }
      },
      onFinish: (evt) => {
        if (!isPinch.value && evt.numberOfPointers < 2) {
          const finalXPosition = evt.translationX - evt.velocityX * 0.3;
          const finalYPosition = evt.translationY + evt.velocityY * 0.1;
          if (
            index.value < imageLength - 1 &&
            Math.abs(halfScreenWidth * (scale.value - 1) + offsetX.value) < 3 &&
            translateX.value < 0 &&
            finalXPosition < -halfScreenWidth &&
            isSwiping.value === IsSwiping.TRUE
          ) {
            cancelAnimation(translationX);
            translationX.value = withTiming(
              -(screenWidth + MARGIN) * (index.value + 1),
              {
                duration: 200,
                easing: Easing.out(Easing.ease),
              },
              () => {
                resetMovementValues();
                index.value = index.value + 1;
                runOnJS(setSelectedIndex)(index.value);
              }
            );
          } else if (
            index.value > 0 &&
            Math.abs(-halfScreenWidth * (scale.value - 1) + offsetX.value) <
              3 &&
            translateX.value > 0 &&
            finalXPosition > halfScreenWidth &&
            isSwiping.value === IsSwiping.TRUE
          ) {
            cancelAnimation(translationX);
            translationX.value = withTiming(
              -(screenWidth + MARGIN) * (index.value - 1),
              {
                duration: 200,
                easing: Easing.out(Easing.ease),
              },
              () => {
                resetMovementValues();
                index.value = index.value - 1;
                runOnJS(setSelectedIndex)(index.value);
              }
            );
          }
          translateX.value =
            scale.value < 1
              ? withTiming(0)
              : translateX.value > halfScreenWidth * (scale.value - 1)
              ? withTiming(halfScreenWidth * (scale.value - 1), {
                  duration: 200,
                })
              : translateX.value < -halfScreenWidth * (scale.value - 1)
              ? withTiming(-halfScreenWidth * (scale.value - 1), {
                  duration: 200,
                })
              : withDecay({
                  clamp: [
                    -halfScreenWidth * (scale.value - 1),
                    halfScreenWidth * (scale.value - 1),
                  ],
                  deceleration: 0.99,
                  velocity: -evt.velocityX,
                });
          translateY.value =
            currentImageHeight * scale.value < screenHeight
              ? withTiming(0)
              : translateY.value >
                (currentImageHeight / 2) * scale.value - halfScreenHeight
              ? withTiming(
                  (currentImageHeight / 2) * scale.value - halfScreenHeight
                )
              : translateY.value <
                (-currentImageHeight / 2) * scale.value + halfScreenHeight
              ? withTiming(
                  (-currentImageHeight / 2) * scale.value + halfScreenHeight
                )
              : withDecay({
                  clamp: [
                    (-currentImageHeight / 2) * scale.value + halfScreenHeight,
                    (currentImageHeight / 2) * scale.value - halfScreenHeight,
                  ],
                  deceleration: 0.99,
                  velocity: evt.velocityY,
                });
          resetTouchValues();
          scale.value =
            scale.value !== offsetScale.value
              ? withTiming(offsetScale.value)
              : offsetScale.value;
          if (
            finalYPosition > halfScreenHeight &&
            offsetY.value + 8 >=
              (currentImageHeight / 2) * scale.value - halfScreenHeight &&
            isSwiping.value !== IsSwiping.TRUE &&
            translateY.value !== 0 &&
            !(
              Math.abs(halfScreenWidth * (scale.value - 1) + offsetX.value) <
                3 &&
              translateX.value < 0 &&
              finalXPosition < -halfScreenWidth
            ) &&
            !(
              Math.abs(-halfScreenWidth * (scale.value - 1) + offsetX.value) <
                3 &&
              translateX.value > 0 &&
              finalXPosition > halfScreenWidth
            )
          ) {
            cancelAnimation(translateX);
            cancelAnimation(translateY);
            cancelAnimation(scale);
            overlayOpacity.value = withTiming(
              0,
              {
                duration: 200,
                easing: Easing.out(Easing.ease),
              },
              () => {
                runOnJS(setOverlay)("none");
              }
            );
            scale.value = withTiming(0.6, {
              duration: 200,
              easing: Easing.out(Easing.ease),
            });
            translateY.value =
              evt.velocityY > 1000
                ? withDecay({
                    velocity: evt.velocityY,
                  })
                : withTiming(
                    halfScreenHeight + (currentImageHeight / 2) * scale.value,
                    {
                      duration: 200,
                      easing: Easing.out(Easing.ease),
                    }
                  );
            translateX.value = withDecay({
              velocity: -evt.velocityX,
            });
          }
        }
      },
      onStart: () => {
        if (!isPinch.value) {
          cancelAnimation(translateX);
          cancelAnimation(translateY);
          cancelAnimation(scale);
          offsetX.value = translateX.value;
          offsetY.value = translateY.value;
        }

        hasPinched.value = HasPinched.FALSE;
      },
    },
    [currentImageHeight]
  );

  const onPinch = useAnimatedGestureHandler(
    {
      onActive: (evt) => {
        if (!isPinch.value && isAndroid) {
          hasPinched.value = HasPinched.TRUE;

          cancelAnimation(translateX);
          cancelAnimation(translateY);
          cancelAnimation(scale);

          isSwiping.value = IsSwiping.UNDETERMINED;

          numberOfPinchFingers.value = evt.numberOfPointers;
          offsetX.value = translateX.value;
          offsetY.value = translateY.value;
          adjustedFocalX.value = evt.focalX - (halfScreenWidth - offsetX.value);
          adjustedFocalY.value =
            evt.focalY - (halfScreenHeight + offsetY.value);
          originX.value = adjustedFocalX.value;
          originY.value = adjustedFocalY.value;
          offsetScale.value = scale.value;
        }

        isPinch.value = true;

        scale.value = clamp(offsetScale.value * evt.scale, 1, 8);
        const localEvtScale = scale.value / offsetScale.value;
        if (scale.value !== 8 && scale.value !== 1) {
          hasHitTopScale.value = 0;
          hasHitBottomScale.value = 0;
        } else if (scale.value === 8 && hasHitTopScale.value === 0) {
          hasHitTopScale.value = 1;
        } else if (scale.value === 1 && hasHitBottomScale.value === 0) {
          hasHitBottomScale.value = 1;
        }
        adjustedFocalX.value = evt.focalX - (halfScreenWidth - offsetX.value);
        adjustedFocalY.value = evt.focalY - (halfScreenHeight + offsetY.value);

        if (numberOfPinchFingers.value !== evt.numberOfPointers) {
          numberOfPinchFingers.value = evt.numberOfPointers;
          if (evt.numberOfPointers === 1) {
            focalOffsetX.value = oldFocalX.value - adjustedFocalX.value;
            focalOffsetY.value = oldFocalY.value - adjustedFocalY.value;
          } else if (numberOfPinchFingers.value > 1) {
            originX.value =
              originX.value -
              (oldFocalX.value / localEvtScale -
                adjustedFocalX.value / localEvtScale);
            originY.value =
              originY.value -
              (oldFocalY.value / localEvtScale -
                adjustedFocalY.value / localEvtScale);
          }
        }

        if (numberOfPinchFingers.value === 1) {
          oldFocalX.value = adjustedFocalX.value + focalOffsetX.value;
          oldFocalY.value = adjustedFocalY.value + focalOffsetY.value;
          translateX.value =
            offsetX.value - oldFocalX.value + localEvtScale * originX.value;
          translateY.value =
            offsetY.value + oldFocalY.value - localEvtScale * originY.value;
        } else if (numberOfPinchFingers.value > 1) {
          oldFocalX.value = adjustedFocalX.value;
          oldFocalY.value = adjustedFocalY.value;
          translateX.value =
            offsetX.value -
            adjustedFocalX.value +
            localEvtScale * originX.value;
          translateY.value =
            offsetY.value +
            adjustedFocalY.value -
            localEvtScale * originY.value;
        }
        headerFooterVisible.value = withTiming(0);
      },
      onFinish: () => {
        if (isPinch.value) {
          translateX.value =
            scale.value < 1
              ? withTiming(0)
              : translateX.value > halfScreenWidth * (scale.value - 1)
              ? withTiming(halfScreenWidth * (scale.value - 1))
              : translateX.value < -halfScreenWidth * (scale.value - 1)
              ? withTiming(-halfScreenWidth * (scale.value - 1))
              : translateX.value;

          translateY.value =
            currentImageHeight * scale.value < screenHeight
              ? withTiming(0)
              : translateY.value >
                (currentImageHeight / 2) * scale.value - screenHeight / 2
              ? withTiming(
                  (currentImageHeight / 2) * scale.value - screenHeight / 2
                )
              : translateY.value <
                (-currentImageHeight / 2) * scale.value + screenHeight / 2
              ? withTiming(
                  (-currentImageHeight / 2) * scale.value + screenHeight / 2
                )
              : translateY.value;

          offsetScale.value = scale.value < 1 ? 1 : scale.value;
          scale.value = scale.value < 1 ? withTiming(1) : scale.value;
          resetTouchValues();
        }
      },
      onStart: (evt) => {
        if (!isAndroid) {
          cancelAnimation(translateX);
          cancelAnimation(translateY);
          cancelAnimation(scale);

          isPinch.value = true;

          isSwiping.value = IsSwiping.UNDETERMINED;

          numberOfPinchFingers.value = evt.numberOfPointers;
          offsetX.value = translateX.value;
          offsetY.value = translateY.value;
          adjustedFocalX.value = evt.focalX - (halfScreenWidth - offsetX.value);
          adjustedFocalY.value =
            evt.focalY - (halfScreenHeight + offsetY.value);
          originX.value = adjustedFocalX.value;
          originY.value = adjustedFocalY.value;
          offsetScale.value = scale.value;
        }

        hasPinched.value = HasPinched.FALSE;
      },
    },
    [currentImageHeight]
  );

  const onSingleTap = useAnimatedGestureHandler({
    onActive: () => {
      cancelAnimation(headerFooterVisible);
      headerFooterVisible.value =
        headerFooterVisible.value > 0 ? withTiming(0) : withTiming(1);
    },
  });

  return { onPinch, onSingleTap, onPan };
};

export const clamp = (value, lowerBound, upperBound) => {
  "worklet";
  return Math.min(Math.max(lowerBound, value), upperBound);
};
