import React, { useContext, useState } from "react";

export const AppContext = React.createContext({});

export const AppProvider = ({ children }) => {
  const [isConnection, setConnection] = useState(false);

  const value = {
    isConnection,
    setConnection,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

export const useAppContext = () => {
  const context = useContext(AppContext);

  return context;
};
