import React from "react";
import { Image } from "react-native";
import Svg, { Path } from "react-native-svg";

export const ArrowUp = () => (
  <Image source={require("../../assets/arrow_up.png")} />
);
