import React, { useContext } from "react";

export const OverlayContext = React.createContext();

export const useOverlayContext = () => {
  const contextValue = useContext(OverlayContext);

  if (contextValue === {}) {
    throw Error(`Error you wrap OverlayProvider`);
  }

  return contextValue;
};
