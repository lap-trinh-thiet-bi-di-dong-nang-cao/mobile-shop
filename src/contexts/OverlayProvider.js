import React, { PropsWithChildren, useEffect, useRef, useState } from "react";

import { BackHandler, Dimensions, StyleSheet } from "react-native";
import Animated, {
  cancelAnimation,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import { ImageGallery } from "../components/ImageGallery";
import { OverlayBackdrop } from "../components/OverlayBackdrop";
import { ImageGalleryProvider } from "./ImageGalleryContext";
import { OverlayContext } from "./OverlayContext";

export const OverlayProvider = (props) => {
  const { children } = props;
  const [overlay, setOverlay] = useState("none");
  const overlayOpacity = useSharedValue(0);
  const { height, width } = Dimensions.get("screen");

  useEffect(() => {
    const backAction = () => {
      if (overlay !== "none") {
        setOverlay("none");
        return true;
      }

      return false;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [overlay]);

  useEffect(() => {
    cancelAnimation(overlayOpacity);
    if (overlay !== "none") {
      overlayOpacity.value = withTiming(1);
    } else {
      overlayOpacity.value = withTiming(0);
    }
  }, [overlay]);

  const overlayStyle = useAnimatedStyle(
    () => ({
      opacity: overlayOpacity.value,
    }),
    []
  );

  const overlayContext = {
    overlay,
    setOverlay,
  };

  return (
    <OverlayContext.Provider value={overlayContext}>
      <ImageGalleryProvider>
        {children}
        <Animated.View
          pointerEvents={overlay === "none" ? "none" : "auto"}
          style={[StyleSheet.absoluteFill, overlayStyle]}
        >
          <OverlayBackdrop
            style={[StyleSheet.absoluteFill, { height, width }]}
          />
        </Animated.View>
        {overlay === "gallery" && (
          <ImageGallery overlayOpacity={overlayOpacity} />
        )}
      </ImageGalleryProvider>
    </OverlayContext.Provider>
  );
};
