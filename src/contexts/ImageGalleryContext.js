import React, { useContext, useState } from "react";

export const ImageGalleryContext = React.createContext({});

export const ImageGalleryProvider = ({ children }) => {
  const [images, setImages] = useState([]);
  const [name, setName] = useState("Unknown product");

  const value = {
    images,
    setImages,
    name,
    setName,
  };
  return (
    <ImageGalleryContext.Provider value={value}>
      {children}
    </ImageGalleryContext.Provider>
  );
};

export const useImageGalleryContext = () => {
  const context = useContext(ImageGalleryContext);

  return context;
};
