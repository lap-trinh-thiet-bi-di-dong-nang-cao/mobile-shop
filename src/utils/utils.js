import { Dimensions } from "react-native";

export const vh = (percentageHeight, rounded = false) => {
  const height = Dimensions.get("window").height;

  const value = height * (percentageHeight / 100);
  return rounded ? Math.round(value) : value;
};

export const vw = (percentageWidth, rounded = false) => {
  const value = Dimensions.get("window").width * (percentageWidth / 100);
  return rounded ? Math.round(value) : value;
};

export const data = [
  {
    id: 1,
    price: 299.99,
    name: "Galaxy Note 100",
    rating: 4.9,
    colors: [
      { color: "red", bonus: 0 },
      { color: "yellow", bonus: 5 },
      { color: "black", bonus: 5 },
      { color: "white", bonus: 5 },
    ],
    storages: [
      { vol: "16GB", bonus: 0 },
      { vol: "32GB", bonus: 30 },
      { vol: "64GB", bonus: 35 },
    ],
    images: [
      require("../../assets/realme_blue.jpeg"),
      require("../../assets/iphone14_xanh.jpeg"),
      require("../../assets/realme-5i.jpg"),
    ],
    originalPrice: 1101232,
  },
  {
    id: 2,
    price: 300.99,
    name: "OPPO Reno8",
    rating: 4.2,
    colors: [
      { color: "yellow", bonus: 4 },
      { color: "black", bonus: 5 },
    ],
    storages: [
      { vol: "128GB", bonus: 25 },
      { vol: "256GB", bonus: 35 },
    ],
    images: [
      require("../../assets/oppo_reno8_4g_gold.png"),
      require("../../assets/oppo_reno8_4g_black.png"),
    ],
  },
  {
    id: 3,
    price: 300.99,
    name: "Xiaomi 12T",
    rating: 4.7,
    colors: [
      { color: "cyan", bonus: 2 },
      { color: "gray", bonus: 2 },
      { color: "black", bonus: 3 },
    ],
    storages: [
      { vol: "128GB", bonus: 15 },
      { vol: "256GB", bonus: 22 },
    ],
    images: [
      require("../../assets/xiaomi-12t-xanh.jpg"),
      require("../../assets/xiaomi-12t-xam.jpg"),
      require("../../assets/xiaomi-12t-den.jpg"),
    ],
  },
  {
    id: 4,
    price: 940.5,
    name: "Galaxy S22 Ultra",
    rating: 4.5,
    colors: [
      { color: "pink", bonus: 7 },
      { color: "white", bonus: 5 },
      { color: "black", bonus: 5 },
    ],
    storages: [
      { vol: "128GB", bonus: 20 },
      { vol: "256GB", bonus: 25 },
      { vol: "512GB", bonus: 30 },
    ],
    images: [
      require("../../assets/s22ultra_white.jpg"),
      require("../../assets/s22ultra_pink.jpg"),
      require("../../assets/s22ultra_black.jpg"),
    ],
  },
  {
    id: 5,
    price: 910,
    name: "Galaxy Z Flip4",
    rating: 4.0,
    colors: [
      { color: "violet", bonus: 9 },
      { color: "black", bonus: 6 },
      { color: "yellow", bonus: 6 },
    ],
    storages: [
      { vol: "256GB", bonus: 27 },
      { vol: "512GB", bonus: 34 },
    ],
    images: [
      require("../../assets/z_flip_yellow.jpg"),
      require("../../assets/z_flip_violet.png"),
      require("../../assets/z_flip_black.jpg"),
    ],
  },
  {
    id: 6,
    price: 402,
    name: "Vivo V25 5G",
    rating: 3.9,
    colors: [
      { color: "black", bonus: 2 },
      { color: "yellow", bonus: 2 },
    ],
    storages: [
      { vol: "256GB", bonus: 18 },
      { vol: "128GB", bonus: 13 },
    ],
    images: [
      require("../../assets/vivo_v25_5g_yellow.png"),
      require("../../assets/vivo_v25_5g_black.png"),
    ],
  },
  {
    id: 7,
    price: 685,
    name: "Oneplus 10T 5G",
    rating: 3.8,
    colors: [{ color: "green", bonus: 6 }],
    storages: [
      { vol: "256GB", bonus: 16 },
      { vol: "128GB", bonus: 13 },
    ],
    images: [require("../../assets/oneplus10t_green.jpg")],
  },
  {
    id: 8,
    price: 910,
    name: "Galaxy Z Flip4",
    rating: 4.0,
    colors: [
      { color: "violet", bonus: 9 },
      { color: "black", bonus: 6 },
      { color: "yellow", bonus: 6 },
    ],
    storages: [
      { vol: "256GB", bonus: 27 },
      { vol: "512GB", bonus: 34 },
    ],
    images: [
      require("../../assets/z_flip_yellow.jpg"),
      require("../../assets/z_flip_violet.png"),
      require("../../assets/z_flip_black.jpg"),
    ],
  },
];
