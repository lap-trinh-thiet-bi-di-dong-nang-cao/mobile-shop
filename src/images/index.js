const images = {
    facebook: require("./facebook.png"),
    google: require("./google.png"),
    apple: require("./apple.png"),
    OPT: require("./Enter-OTP.png"),
    forgotPass: require("./Forgot-password.png"),
    resetPass: require("./Reset-password.png"),
};

export default images;
