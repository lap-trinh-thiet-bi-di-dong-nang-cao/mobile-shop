import { StyleSheet, TextInput, TouchableOpacity, View } from "react-native";

export const HInput = ({
    inputRef,
    onChangeText = () => {},
    style,
    borderWidth = 0,
    textAlign = "left",
    borderColor = "white",
    bgColor = "white",
    radius = 0,
    fontSize = 16,
    isPassword = false,
    startIcon,
    endIcon,
    placeholder = "",
    maxLength = 256,
    handleEndIcon = () => {},
}) => {
    return (
        <View
            style={[
                styles.wrapper,
                {
                    borderWidth,
                    borderColor,
                    borderRadius: radius,
                    backgroundColor: bgColor,
                },
                style,
            ]}
        >
            {startIcon != null ? startIcon : <></>}

            <TextInput
                ref={(r) => (inputRef = r)}
                secureTextEntry={isPassword}
                style={[{ fontSize, textAlign }, styles.input]}
                placeholder={placeholder}
                maxLength={maxLength}
                onChangeText={onChangeText}
            />

            {endIcon != null ? (
                <TouchableOpacity
                    style={styles.endIcon}
                    onPress={handleEndIcon}
                >
                    {endIcon}
                </TouchableOpacity>
            ) : (
                <></>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        // flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        flex: 1,
        marginHorizontal: 8,
        // backgroundColor: "red",
    },
});
