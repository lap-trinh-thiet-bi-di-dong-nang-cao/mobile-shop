import React from "react";
import { Image, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Banner } from "../icons/Banner";
import { Place } from "../icons/Place";
import { Star } from "../icons/Star";

export const ProductItemContext = (props) => {
  const { item, onPress } = props;
  const Stars = () => {
    return (
      <View style={{ flexDirection: "row" }}>
        <Star />
        <Star />
        <Star />
        <Star />
        <Star />
      </View>
    );
  };

  return (
    <View
      style={{
        width: "46%",
        backgroundColor: "white",
        marginVertical: 5,
        borderRadius: 5,
        shadowColor: "#999999",
        ...Platform.select({
          android: {
            elevation: 4,
          },
          default: {
            shadowOffset: {
              height: 3,
              width: 0,
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
          },
        }),
      }}
    >
      <TouchableOpacity
        onPress={() => {
          onPress?.();
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <Image
            source={item.images[0]}
            style={{
              flex: 1,
              height: 150,
              alignItems: "flex-end",
            }}
            resizeMode={"contain"}
          />
        </View>
        <View style={{ position: "absolute", top: 0, left: -3 }}>
          <Banner>
            <Text style={{ fontSize: 10, color: "white", fontWeight: "bold" }}>
              Giảm 14%
            </Text>
          </Banner>
        </View>
        <View style={{ padding: 8 }}>
          <View>
            <Text
              style={{ fontSize: 11, fontWeight: "bold", color: "#333" }}
              numberOfLines={2}
            >
              {item.name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              marginTop: 5,
              flexWrap: "wrap",
              alignItems: "flex-end",
            }}
          >
            <Text
              style={{ fontSize: 14, color: "#2C5BF6", fontWeight: "bold" }}
            >
              $ {item.price}
            </Text>
            <Text
              style={{
                fontSize: 8,
                color: "#666",
                fontWeight: "bold",
                textDecorationLine: "line-through",
                marginLeft: 3,
              }}
            >
              $ {item.price}
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 5 }}>
            <Stars />
            <Text style={{ fontSize: 8, marginLeft: 3 }}>Đã bán 5</Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 5 }}>
            <Place width={10} height={10} fill={"#ccc"} />
            <Text style={{ fontSize: 8, marginLeft: 3, color: "#666" }}>
              Đã bán 5
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const areEqual = (prevProps, nextProps) => {
  const { item: prevItem } = prevProps;
  const { item: nextItem } = nextProps;

  const thumbnailEqual = prevItem.thumbnail === nextItem.thumbnail;

  if (!thumbnailEqual) return false;

  const nameEqual = prevItem.name === nextItem.name;

  if (!nameEqual) return false;

  const priceEqual = prevItem.price === nextItem.price;

  if (!priceEqual) return false;
  return true;
};

const MemoizedMessage = React.memo(ProductItemContext, areEqual);

export const ProductItem = (props) => {
  return <MemoizedMessage {...props} />;
};
