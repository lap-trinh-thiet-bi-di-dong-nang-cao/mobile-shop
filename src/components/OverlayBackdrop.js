import React from "react";
import { StyleProp, View, ViewStyle } from "react-native";

export const OverlayBackdrop = (props) => {
  const { style } = props;
  return <View style={[{ backgroundColor: "#000000CC" }, style]} />;
};
