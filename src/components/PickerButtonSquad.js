import { AntDesign, Entypo } from "@expo/vector-icons";
import { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

function PickerButtonSquad({ text = "0GB", active = false, ...passProp }) {
  // const [selected, setSelected] = useState(active);
  return (
    <TouchableOpacity
      style={[
        styles.wrapper,
        { borderColor: active == true ? "#2C5BF6" : "gray" },
      ]}
      {...passProp}
    >
      <Text style={[styles.dot]}>{text}</Text>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  wrapper: {
    width: 80,
    height: 50,
    borderWidth: 2,
    borderColor: "#f2f2f2",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    borderRadius: 5,
  },
  dot: {
    fontSize: 18,
  },
});
export default PickerButtonSquad;
