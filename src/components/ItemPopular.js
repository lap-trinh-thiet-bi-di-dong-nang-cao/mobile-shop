import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import Ratingbar from "./Ratingbar";

function ItemPopular({ item, onPress }) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View
        style={{
          width: "100%",
          height: 20,
          flexDirection: "row",
          justifyContent: "flex-end",
        }}
      >
        <AntDesign style={styles.icon} name="hearto" size={20} />
      </View>
      <Image style={styles.avatar} source={item.images[0]} />

      <View style={styles.content}>
        <Text style={styles.name}>{item.name}</Text>

        <Ratingbar rating={item.rating} reviewerNumber={40}></Ratingbar>

        <View style={styles.price}>
          <Text
            style={{
              fontSize: 12,
              textDecorationLine: "line-through",
              color: "#999999",
            }}
          >
            $ {item.originalPrice}
          </Text>
          <Text style={{ fontWeight: "500", marginLeft: 10, color: "#2C5BF6" }}>
            $ {item.price}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 150,
    height: 190,
    padding: 8,
    marginRight: 10,
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "#a6a6a6",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    //elevation: 5,
  },
  avatar: {
    width: "100%",
    height: 80,
    resizeMode: "contain",
  },
  content: {
    width: "100%",
    height: 76,
  },
  name: {
    fontSize: 16,
    fontWeight: "500",
  },
  rating: {
    flexDirection: "row",
    marginTop: 3,
  },
  price: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 0,
  },
  icon: {
    color: "#b3b3b3",
  },
});

export default ItemPopular;
