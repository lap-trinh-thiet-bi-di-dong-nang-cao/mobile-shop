import React, { useEffect, useRef, useState } from "react";
import {
  Dimensions,
  Image,
  ImageStyle,
  Keyboard,
  Platform,
  StatusBar,
  StyleSheet,
  ViewStyle,
} from "react-native";

import {
  PanGestureHandler,
  PinchGestureHandler,
  TapGestureHandler,
} from "react-native-gesture-handler";

import Animated, {
  cancelAnimation,
  Easing,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import { useImageGalleryContext } from "../contexts/ImageGalleryContext";
import { useOverlayContext } from "../contexts/OverlayContext";
import { useImageGalleryGestureHandler } from "../hooks/useImageGalleryGestureHandler";
import { vh, vw } from "../utils/utils";
import { AnimatedGalleryImage } from "./AnimatedGalleryImage";
import { ImageGalleryHeader } from "./ImageGalleryHeader";

// const images = [
//   "https://cdn2.cellphones.com.vn/358x/media/catalog/product/t/_/t_m_18.png",
//   "https://cdn2.cellphones.com.vn/358x/media/catalog/product/b/_/b_c_1_9.png",
//   "https://cdn2.cellphones.com.vn/358x/media/catalog/product/x/_/x_m_24.png",
//   "https://cdn2.cellphones.com.vn/358x/media/catalog/product/v/_/v_ng_18.png",
// ];

const isAndroid = Platform.OS === "android";
const screenWidth = vw(100);
const fullScreenHeight = Dimensions.get("screen").height;
const measuredScreenHeight = vh(100);
const halfScreenWidth = vw(50);
const MARGIN = 32;

export const ImageGallery = (props) => {
  const { overlayOpacity } = props;

  const { overlay } = useOverlayContext();

  const { images } = useImageGalleryContext();

  const statusBarHeight = StatusBar.currentHeight ?? 0;
  const bottomBarHeight =
    fullScreenHeight - measuredScreenHeight - statusBarHeight;
  const androidScreenHeightAdjustment =
    bottomBarHeight === statusBarHeight || bottomBarHeight < 0
      ? -statusBarHeight
      : 0;
  const screenHeight = isAndroid
    ? Dimensions.get("window").height + androidScreenHeightAdjustment
    : vh(100);
  const halfScreenHeight = screenHeight / 2;
  const quarterScreenHeight = screenHeight / 4;

  const screenTranslateY = useSharedValue(screenHeight);
  const showScreen = () => {
    "worklet";
    screenTranslateY.value = withTiming(0, {
      duration: 250,
      easing: Easing.out(Easing.ease),
    });
  };

  useEffect(() => {
    Keyboard.dismiss();
    showScreen();
  }, []);

  const [currentImageHeight, setCurrentImageHeight] = useState(screenHeight);

  const [selectedIndex, setSelectedIndex] = useState(0);

  const headerFooterVisible = useSharedValue(1);

  const panRef = useRef(null);
  const pinchRef = useRef(null);
  const singleTapRef = useRef(null);

  const translateX = useSharedValue(0);
  const translateY = useSharedValue(0);
  const offsetScale = useSharedValue(1);
  const scale = useSharedValue(1);
  const translationX = useSharedValue(0);

  const { onPinch, onSingleTap, onPan } = useImageGalleryGestureHandler({
    currentImageHeight,
    halfScreenHeight,
    halfScreenWidth,
    headerFooterVisible,
    offsetScale,
    overlayOpacity,
    scale,
    screenHeight,
    screenWidth,
    selectedIndex,
    setSelectedIndex,
    translateX,
    translateY,
    translationX,
    imageLength: images.length,
  });

  const headerFooterOpacity = useDerivedValue(
    () =>
      currentImageHeight * scale.value < screenHeight && translateY.value > 0
        ? 1 - translateY.value / quarterScreenHeight
        : currentImageHeight * scale.value > screenHeight &&
          translateY.value >
            (currentImageHeight / 2) * scale.value - halfScreenHeight
        ? 1 -
          (translateY.value -
            ((currentImageHeight / 2) * scale.value - halfScreenHeight)) /
            quarterScreenHeight
        : 1,
    [currentImageHeight]
  );

  const pagerStyle = useAnimatedStyle(
    () => ({
      transform: [
        { scaleX: -1 },
        {
          translateX: translationX.value,
        },
      ],
    }),
    []
  );

  const containerBackground = useAnimatedStyle(
    () => ({
      backgroundColor: "#FCFCFC",
      opacity: headerFooterOpacity.value,
    }),
    [headerFooterOpacity]
  );

  const showScreenStyle = useAnimatedStyle(
    () => ({
      transform: [
        {
          translateY: screenTranslateY.value,
        },
      ],
    }),
    []
  );

  return (
    <Animated.View
      pointerEvents={"auto"}
      style={[StyleSheet.absoluteFillObject, showScreenStyle]}
    >
      <Animated.View
        style={[StyleSheet.absoluteFillObject, containerBackground]}
      />
      <TapGestureHandler
        minPointers={1}
        numberOfTaps={1}
        onGestureEvent={onSingleTap}
        ref={singleTapRef}
        waitFor={[panRef, pinchRef]}
      >
        <Animated.View style={[StyleSheet.absoluteFillObject]}>
          <PinchGestureHandler
            onGestureEvent={onPinch}
            ref={pinchRef}
            simultaneousHandlers={[panRef]}
          >
            <Animated.View style={StyleSheet.absoluteFill}>
              <PanGestureHandler
                enabled={overlay === "gallery"}
                maxPointers={isAndroid ? undefined : 1}
                minDist={10}
                ref={panRef}
                onGestureEvent={onPan}
                simultaneousHandlers={[pinchRef]}
              >
                <Animated.View style={StyleSheet.absoluteFill}>
                  <Animated.View
                    style={[
                      styles.animatedContainer,
                      pagerStyle,
                      {
                        transform: [
                          { scaleX: -1 },
                          {
                            translateX: translationX.value,
                          },
                        ],
                      },
                    ]}
                  >
                    {images.map((value, index) => {
                      return (
                        <AnimatedGalleryImage
                          index={index}
                          image={value}
                          shouldRender={Math.abs(selectedIndex - index) < 4}
                          source={{ uri: value }}
                          style={[
                            {
                              height: screenHeight * 8,
                              marginRight: MARGIN,
                              width: screenWidth * 8,
                            },
                          ]}
                          translateX={translateX}
                          translateY={translateY}
                          scale={scale}
                          screenHeight={screenHeight}
                          selected={selectedIndex === index}
                          previous={selectedIndex > index}
                          key={`${value}-${index}`}
                          offsetScale={offsetScale}
                        />
                      );
                    })}
                  </Animated.View>
                </Animated.View>
              </PanGestureHandler>
            </Animated.View>
          </PinchGestureHandler>
        </Animated.View>
      </TapGestureHandler>
      <ImageGalleryHeader
        opacity={headerFooterOpacity}
        visible={headerFooterVisible}
      />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  animatedContainer: {
    alignItems: "center",
    flexDirection: "row",
  },
});
