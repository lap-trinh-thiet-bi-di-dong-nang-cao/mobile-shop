import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import Ratingbar from "./Ratingbar";

function ItemSale({ item, onPress }) {
  return (
    <View
      style={[
        styles.container,
        {
          ...Platform.select({
            android: {
              elevation: 4,
            },
            default: {
              shadowOffset: {
                height: 3,
                width: 0,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
            },
          }),
        },
      ]}
    >
      <Image style={styles.avatar} source={item.images[0]} />

      <View style={styles.content}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View>
            <Text style={styles.name}>{item.name}</Text>
            <Ratingbar rating={item.rating} reviewerNumber={40}></Ratingbar>
          </View>

          <AntDesign style={styles.icon} name="hearto" size={20} />
        </View>

        <View style={styles.price}>
          <Text
            style={{
              fontSize: 12,
              textDecorationLine: "line-through",
              color: "#999999",
            }}
          >
            $ {item.originalPrice}
          </Text>
          <Text style={{ fontWeight: "500", marginLeft: 10, color: "#2C5BF6" }}>
            $ {item.price}
          </Text>
        </View>
        <TouchableOpacity onPress={onPress}>
          <AntDesign
            style={{
              position: "absolute",
              bottom: 0,
              right: 0,
            }}
            name="arrowright"
            size={24}
            color="black"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 100,
    flexDirection: "row",
    backgroundColor: "white",
    padding: 10,
    marginBottom: 10,
    borderRadius: 10,
    backgroundColor: "white",
  },
  avatar: {
    width: "35%",
    height: "100%",
    resizeMode: "contain",
  },
  content: {
    width: "65%",
    height: "100%",
    justifyContent: "space-between",
  },
  name: {
    fontSize: 16,
    fontWeight: "500",
  },
  rating: {
    flexDirection: "row",
    marginTop: 3,
  },
  price: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 2,
  },
  icon: {
    color: "#b3b3b3",
  },
});

export default ItemSale;
