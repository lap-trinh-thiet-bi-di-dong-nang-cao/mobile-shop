import React, { useEffect, useMemo } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Animated, {
  useAnimatedReaction,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withSpring,
  withTiming,
} from "react-native-reanimated";
import { ArrowUp } from "../icons/ArrowUp";

export const ScrollToBottomButton = (props) => {
  const { onPress, showNotification = true } = props;

  const progress = useSharedValue(0);

  const scrollButtonRStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: progress.value,
        },
      ],
    };
  });

  useDerivedValue(() => {
    if (showNotification) {
      progress.value = withTiming(1);
    } else {
      progress.value = withTiming(0, {
        duration: 100,
      });
    }
  }, [showNotification]);

  return (
    <TouchableOpacity onPress={onPress} style={[styles.touchable]}>
      <View style={styles.wrapper}>
        <Animated.View style={[styles.container, scrollButtonRStyle]}>
          <ArrowUp />
        </Animated.View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    borderRadius: 20,
    elevation: 5,
    height: 40,
    justifyContent: "center",
    shadowOffset: {
      height: 4,
      width: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    width: 40,
    backgroundColor: "#fff",
    shadowColor: "#333",
  },
  touchable: {
    bottom: 40,
    position: "absolute",
    right: 20,
  },
  wrapper: {
    alignItems: "center",
    height: 50,
    justifyContent: "flex-end",
  },
});
