import React from "react";
import { StyleSheet, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const Button = (props) => {
  const { title, color = "white", onPress, style } = props;
  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
      <Text style={[styles.title, { color: color }]}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#1A74E5",
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  title: {
    color: "white",
    fontWeight: "600",
  },
});

export default Button;
