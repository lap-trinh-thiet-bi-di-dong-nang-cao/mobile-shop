import { StyleSheet, View, Text } from "react-native";
import { AntDesign } from "@expo/vector-icons";

function Ratingbar({ rating = 3, reviewerNumber = 30 }) {
  return (
    <View style={styles.rating}>
      <AntDesign
        name="star"
        size={16}
        color={rating >= 1 ? "yellow" : "#b3b3b3"}
      />
      <AntDesign
        name="star"
        size={16}
        color={rating >= 2 ? "yellow" : "#b3b3b3"}
      />
      <AntDesign
        name="star"
        size={16}
        color={rating >= 3 ? "yellow" : "#b3b3b3"}
      />
      <AntDesign
        name="star"
        size={16}
        color={rating >= 4 ? "yellow" : "#b3b3b3"}
      />
      <AntDesign
        name="star"
        size={16}
        color={rating >= 5 ? "yellow" : "#b3b3b3"}
      />
      <Text style={{ marginLeft: 10, color: "#999999" }}>
        ({reviewerNumber})
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  rating: {
    width: "100%",
    height: 24,
    flexDirection: "row",
    marginTop: 3,
  },
});

export default Ratingbar;
