import React from "react";
import { View } from "react-native";
import Animated, { useAnimatedStyle } from "react-native-reanimated";
import { vw } from "../utils/utils";

const screenWidth = vw(100);
const halfScreenWidth = vw(50);
const oneEighth = 1 / 8;

export const AnimatedGalleryImage = React.memo(
  (props) => {
    const {
      index,
      offsetScale,
      image,
      previous,
      scale,
      screenHeight,
      selected,
      shouldRender,
      style,
      translateX,
      translateY,
    } = props;

    const AnimatedGalleryImageStyle = useAnimatedStyle(() => {
      const xScaleOffset = -7 * screenWidth * (0.5 + index);
      const yScaleOffset = -screenHeight * 3.5;
      return {
        transform: [
          {
            translateX: selected
              ? translateX.value + xScaleOffset
              : scale.value < 1 || scale.value !== offsetScale.value
              ? xScaleOffset
              : previous
              ? translateX.value -
                halfScreenWidth * (scale.value - 1) +
                xScaleOffset
              : translateX.value +
                halfScreenWidth * (scale.value - 1) +
                xScaleOffset,
          },
          {
            translateY: selected
              ? translateY.value + yScaleOffset
              : yScaleOffset,
          },
          {
            scale: selected ? scale.value / 8 : oneEighth,
          },
          { scaleX: -1 },
        ],
      };
    }, [previous, selected]);

    if (!shouldRender) {
      return <View style={[style, { transform: [{ scale: oneEighth }] }]} />;
    }

    return (
      <Animated.Image
        resizeMode={"contain"}
        source={image}
        style={[
          style,
          AnimatedGalleryImageStyle,
          {
            transform: [
              { scaleX: -1 },
              { translateY: -screenHeight * 3.5 },
              {
                translateX: -translateX.value + 7 * screenWidth * (0.5 + index),
              },
              { scale: oneEighth },
            ],
          },
        ]}
      />
    );
  },
  (prevProps, nextProps) => {
    if (
      prevProps.selected === nextProps.selected &&
      prevProps.shouldRender === nextProps.shouldRender &&
      prevProps.image === nextProps.image &&
      prevProps.previous === nextProps.previous &&
      prevProps.index === nextProps.index &&
      prevProps.screenHeight === nextProps.screenHeight
    ) {
      return true;
    }
    return false;
  }
);
