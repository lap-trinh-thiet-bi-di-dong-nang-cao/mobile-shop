import React, { useState } from "react";
import {
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from "react-native-reanimated";
import { useImageGalleryContext } from "../contexts/ImageGalleryContext";
import { useOverlayContext } from "../contexts/OverlayContext";
import { Close } from "../icons/Close";

const ReanimatedSafeAreaView = Animated.createAnimatedComponent
  ? Animated.createAnimatedComponent(SafeAreaView)
  : SafeAreaView;

export const ImageGalleryHeader = (props) => {
  const { opacity, visible } = props;
  const [height, setHeight] = useState(200);
  const { setOverlay } = useOverlayContext();
  const { name } = useImageGalleryContext();

  const headerStyle = useAnimatedStyle(() => ({
    opacity: opacity.value,
    transform: [
      {
        translateY: interpolate(
          visible.value,
          [0, 1],
          [-height, 0],
          Extrapolate.CLAMP
        ),
      },
    ],
  }));

  const androidTranslucentHeaderStyle = {
    paddingTop:
      Platform.OS === "android" && true ? StatusBar.currentHeight : undefined,
  };

  const hideOverlay = () => {
    setOverlay("none");
  };

  return (
    <View
      onLayout={(event) => setHeight(event.nativeEvent.layout.height)}
      pointerEvents={"box-none"}
    >
      <ReanimatedSafeAreaView
        style={[
          { backgroundColor: "#fff" },
          androidTranslucentHeaderStyle,
          headerStyle,
        ]}
      >
        <View style={[styles.innerContainer]}>
          <TouchableOpacity
            accessibilityLabel="Hide Overlay"
            onPress={hideOverlay}
          >
            <View style={[styles.leftContainer]}>
              <Close fill={"#333"} width={24} height={24} />
            </View>
          </TouchableOpacity>
          <View style={[styles.centerContainer]}>
            <Text style={[styles.title, { color: "#333" }]}>{name}</Text>
          </View>
          <View style={[styles.rightContainer]} />
        </View>
      </ReanimatedSafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  centerContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  innerContainer: {
    flexDirection: "row",
    height: 56,
  },
  leftContainer: {
    flex: 1,
    justifyContent: "center",
    marginLeft: 8,
  },
  rightContainer: {
    marginRight: 8,
    width: 24,
  },
  title: {
    fontSize: 16,
    fontWeight: "700",
  },
});
