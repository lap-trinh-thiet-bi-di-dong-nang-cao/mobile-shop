import React from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useNavigation } from "@react-navigation/native";
import { GoBack } from "../icons/GoBack";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const BackButton = (props) => {
  const { onBack } = props;

  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.goBack();
        if (onBack) {
          onBack();
        }
      }}
      style={styles.backButton}
    >
      <GoBack />
    </TouchableOpacity>
  );
};

const HEADER_CONTENT_HEIGHT = 55;

const ScreenHeader = (props) => {
  const {
    inSafeArea,
    LeftContent,
    onBack = () => {},
    RightContent = () => <View style={{ height: 24, width: 24 }} />,
    style,
    Subtitle,
    subtitleText,
    Title,
    titleText,
    CenterContent,
    rightWrapContainer,
  } = props;

  const inset = useSafeAreaInsets();
  return (
    <View
      style={[
        styles.safeAreaContainer,
        {
          backgroundColor: "white",
          height: HEADER_CONTENT_HEIGHT + (inSafeArea ? 0 : inset.top),
        },
        style,
      ]}
    >
      <View
        style={[
          styles.contentContainer,
          {
            height: HEADER_CONTENT_HEIGHT,
            marginTop: inSafeArea ? 0 : inset.top,
          },
        ]}
      >
        <View style={styles.leftContainer}>
          {LeftContent ? <LeftContent /> : <BackButton onBack={onBack} />}
        </View>
        <View
          style={[
            styles.centerContainer,
            {
              alignItems: CenterContent ? "flex-start" : "center",
              marginLeft: CenterContent ? 0 : 30,
            },
          ]}
        >
          {CenterContent ? (
            <CenterContent />
          ) : (
            <View>
              <View
                style={{ paddingBottom: !!Subtitle || !!subtitleText ? 3 : 0 }}
              >
                {Title ? (
                  <Title />
                ) : (
                  <Text style={styles.title}>{titleText}</Text>
                )}
              </View>
              {Subtitle ? (
                <Subtitle />
              ) : (
                !!subtitleText && (
                  <Text style={styles.subTitle}>{subtitleText}</Text>
                )
              )}
            </View>
          )}
        </View>
        <View style={[styles.rightContainer, rightWrapContainer]}>
          <RightContent />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  backButton: {
    paddingVertical: 8,
  },
  centerContainer: {
    flex: 1,
    justifyContent: "center",
  },
  contentContainer: {
    alignItems: "center",
    flexDirection: "row",
    padding: 8,
  },
  leftContainer: {
    width: 40,
  },
  rightContainer: {
    alignItems: "flex-end",
    width: 70,
  },
  safeAreaContainer: {
    borderBottomWidth: 1,
    borderBottomColor: "#F1F1F1",
  },
  subTitle: {
    fontSize: 12,
    color: "grey",
  },
  title: {
    fontSize: 16,
    fontWeight: "700",
    color: "black",
  },
});

export default ScreenHeader;
