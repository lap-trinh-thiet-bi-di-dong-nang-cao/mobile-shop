import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

const Tag = ({
  text,
  style,
  highlightSelected = "#2C5BF6",
  isSelected = false,
  textStyle,
  onPress,
  Icon,
}) => {
  return (
    <TouchableOpacity
      style={{
        ...styles.tagContainer,
        ...style,
        borderWidth: isSelected ? 1 : 0,
        borderColor: isSelected ? highlightSelected : null,
        backgroundColor: isSelected ? "#F4F8FF" : "#F3F3F3",
      }}
      onPress={() => {
        if (onPress) {
          onPress();
        }
      }}
    >
      {Icon && <Icon />}
      <Text
        style={{
          fontSize: 11,
          fontWeight: "bold",
          ...textStyle,
          color: isSelected ? highlightSelected : "#333",
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  tagContainer: {
    padding: 5,
    borderRadius: 6,
    margin: 3,
    flexDirection: "row",
    alignItems: "center",
  },
});

export default Tag;
