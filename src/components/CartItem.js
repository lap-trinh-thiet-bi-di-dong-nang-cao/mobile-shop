import react, { useState } from "react";
import { EvilIcons } from "@expo/vector-icons";

import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";

const action = {
  ADD: "+",
  SUB: "-",
};
function CartItem({ item = {}, index, onChange, onRemove, navigation }) {
  const [quantity, setQuantity] = useState(item.quantity);

  const changeQuantity = (c) => {
    if (c == action.ADD) {
      setQuantity((prev) => Number.parseInt(prev) + 1);
      onChange(index, quantity + 1);
    } else {
      setQuantity((prev) => (prev > 0 ? prev - 1 : prev));
      onChange(index, quantity - 1);
    }
  };
  return (
    <View
      style={[
        styles.wrapper,
        {
          ...Platform.select({
            android: {
              elevation: 4,
            },
            default: {
              shadowOffset: {
                height: 3,
                width: 0,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
            },
          }),
        },
      ]}
    >
      <Image
        style={styles.image}
        resizeMode="contain"
        source={item.product.images[0]}
      ></Image>

      <View style={styles.infor}>
        <Text style={styles.textName}>{item.product.name}</Text>
        <View style={styles.btnQuantity}>
          <TouchableOpacity
            onPress={() => changeQuantity(action.SUB)}
            style={[styles.quantity, { borderRightWidth: 0 }]}
          >
            <Text style={{ fontWeight: "bold", color: "#a6a6a6" }}>-</Text>
          </TouchableOpacity>
          <TextInput
            keyboardType="numeric"
            value={quantity + ""}
            style={styles.quantity}
            onChangeText={(text) => setQuantity(Number.parseInt(text))}
          />
          <TouchableOpacity
            onPress={() => changeQuantity(action.ADD)}
            style={[styles.quantity, { borderLeftWidth: 0 }]}
          >
            <Text style={{ fontWeight: "bold", color: "#a6a6a6" }}>+</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={{ flexDirection: "row" }}>
          <EvilIcons name="heart" size={24} color="#a6a6a6" />
          <Text style={{ color: "#a6a6a6" }}>add to favorite</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.right}>
        <Text style={styles.price}>${item.product.price}</Text>
        <TouchableOpacity
          onPress={() => onRemove(index)}
          style={styles.btnRemove}
        >
          <EvilIcons name="trash" size={24} color="#a6a6a6" />
          <Text style={{ color: "#a6a6a6" }}>Remove</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "white",
    height: 150,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
    marginBottom: 20,
    borderRadius: 15,
  },
  image: {
    height: "100%",
    width: "20%",
    flex: 1,
  },
  infor: {
    justifyContent: "space-between",
    flex: 1,
  },
  textName: {
    fontSize: 16,
    fontWeight: "700",
  },
  btnQuantity: {
    flexDirection: "row",
  },
  quantity: {
    color: "#a6a6a6",
    padding: 5,
    width: 30,
    height: 30,
    borderWidth: 1,
    borderColor: "#a6a6a6",
    justifyContent: "center",
    alignItems: "center",
  },
  right: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    width: "20%",
  },
  price: {
    paddingBottom: 30,
    fontSize: 16,
    color: "#3399ff",
    fontWeight: "700",
  },
  btnRemove: {
    flexDirection: "row",
  },
});
export default CartItem;
