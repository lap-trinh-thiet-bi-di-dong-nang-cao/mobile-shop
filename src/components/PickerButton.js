import { StyleSheet, TouchableOpacity, View } from "react-native";

function PickerButton({ color = "black", active = false, ...passProp }) {
  // const [selected, setSelected] = useState(active);
  return (
    <TouchableOpacity
      style={[
        styles.wrapper,
        { borderColor: active == true ? "#2C5BF6" : "gray" },
      ]}
      {...passProp}
    >
      <View style={[styles.dot, { backgroundColor: color }]}></View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  wrapper: {
    width: 40,
    height: 40,
    borderWidth: 2,
    borderColor: "#f2f2f2",
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    // textAlign: "center",
    marginRight: 10,
  },
  dot: {
    width: "60%",
    height: "60%",
    borderRadius: 1000,
    backgroundColor: "black",
  },
});
export default PickerButton;
