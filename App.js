import { NavigationContainer } from "@react-navigation/native";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { View } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { OverlayProvider } from "./src/contexts/OverlayProvider";
import { AppProvider, useAppContext } from "./src/hooks/useAppContext";
import ResultProductScreen from "./src/screens/ResultProductScreen";
import MainScreen from "./src/screens/MainScreen";
import RegisterScreen from "./src/screens/RegisterScreen";
import LoginScreen from "./src/screens/LoginScreen";
import DetailScreen from "./src/screens/DetailScreen";
import ListItemScreen from "./src/screens/ListItemScreen";
import ForgotPassScreen from "./src/screens/ForgotPassScreen";
import OTPScreen from "./src/screens/OTPScreen";
import ResetPassScreen from "./src/screens/ResetPassScreen";

const MainStack = createStackNavigator();

const WelcomeStack = createStackNavigator();

export default function App() {
  return (
    <SafeAreaProvider>
      <AppProvider>
        <OverlayProvider>
          <NavigationComponent />
        </OverlayProvider>
      </AppProvider>
    </SafeAreaProvider>
  );
}

const NavigationComponent = () => {
  const { isConnection } = useAppContext();

  return (
    <NavigationContainer>
      {isConnection ? (
        <MainStack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName={"Main"}
        >
          <MainStack.Screen name="Main" component={MainScreen} />
          <MainStack.Screen name="Detail" component={DetailScreen} />
          <MainStack.Screen
            name="ResultProduct"
            component={ResultProductScreen}
          />
          <MainStack.Screen name="ListItem" component={ListItemScreen} />
        </MainStack.Navigator>
      ) : (
        <WelcomeStack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName="Login"
        >
          <WelcomeStack.Screen name="Login" component={LoginScreen} />
          <WelcomeStack.Screen name="SignUp" component={RegisterScreen} />
          <WelcomeStack.Screen
            name="ForgotPassword"
            component={ForgotPassScreen}
          />
          <WelcomeStack.Screen name="OTPScreen" component={OTPScreen} />
          <WelcomeStack.Screen
            name="ResetPassword"
            component={ResetPassScreen}
          />
        </WelcomeStack.Navigator>
      )}
    </NavigationContainer>
  );
};
